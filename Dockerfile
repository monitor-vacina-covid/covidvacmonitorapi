FROM node:14-alpine AS build
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci
COPY src ./src
COPY tsconfig.json ./tsconfig.json
RUN npm run build

# Run application
FROM node:14-alpine as serve
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY --from=build /usr/src/app/build ./
CMD [ "node", "index.js"]
