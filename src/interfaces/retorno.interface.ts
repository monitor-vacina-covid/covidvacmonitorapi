export interface Retorno<T> {
    sucesso: boolean,
    completo: boolean,
    userErro?: {code?: string, mensagem?: string},
    logErro?: any,
    data: T | any
}