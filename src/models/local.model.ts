import { HTMLElement } from 'node-html-parser';
import { Disponibilidade } from './disponibilidade.model';

export class Local extends Disponibilidade {
    public local: string;
    public value: number;

    constructor(el: HTMLElement) {
        super(el.innerText);
        this.local = this.cleanLocal(el.innerText);
        this.value = Number(el.attrs.value);
    }

    private cleanLocal(local: string) {
        return local.replace(/&nbsp/, '').trim();
    }
}