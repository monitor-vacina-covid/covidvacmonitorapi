import { Retorno } from './../interfaces/retorno.interface';
import { Dose, Grupo } from './../models/grupo.model';
import { getAge } from './../utils/age.util';
import { GrupoService } from './grupo.service';
import { LocalService } from './local.service';
import { LocalGrupo } from './../models/localGrupo.model';
import { AxiosError } from 'axios';
import { ValidationError } from 'fast-xml-parser';


export class LocalGrupoService {
    constructor(
        private localService: LocalService,
        private grupoService: GrupoService
    ) {}

    public async getDisponiveis(): Promise<Retorno<LocalGrupo[]>> {
        const retorno: Retorno<LocalGrupo[]> = {
            sucesso: true,
            completo: true,
            data: [],
        };
        const age = getAge();

        try {
            const locais = await this.localService.getLocais();
        
            for (const ld of locais.filter(l => l.disponivel)) {
                const gruposDoses = await Promise.all([Dose.primeira, Dose.segunda].map(async dose => this.grupoService.getGrupos(dose, age, ld.value)));
                const grupos: Grupo[] = [];
                gruposDoses.forEach(g => {
                    if (g != undefined) {
                        grupos.push(...g);
                    } else {
                        console.log(g)
                        retorno.completo = false;
                    }
                })
                //const grupos = gruposDoses.reduce((a, b) => a.concat(b), [])
                grupos.filter(grupo => grupo.disponivel).forEach(gd => {
                    retorno.data.push(new LocalGrupo(ld, gd));                      
                })
            }

            return retorno;
        } catch(error) {
            retorno.sucesso = false;

            if (error.isAxiosError) {
                const _error = error as AxiosError
                retorno.userErro = {
                    code: _error.code, 
                    mensagem: 'Houve um erro com o servidor de consulta'
                }
                retorno.logErro = JSON.stringify(_error.toJSON());
            } else if (error.err) {
                const _error = error as ValidationError
                retorno.userErro = {
                    mensagem: 'Houve um erro com o retorno do servidor de consulta'
                }
                retorno.logErro = JSON.stringify(_error.err);
            } else {
                retorno.userErro = {
                    mensagem: 'Houve um erro inesperado'
                }

                retorno.logErro = JSON.stringify(error);
            }

            return retorno;
        }
    }
}