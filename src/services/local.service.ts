import { parse } from 'node-html-parser';
import { AxiosInstance } from "axios";
import { Local } from '../models/local.model';

export class LocalService {
    constructor(
        private client: AxiosInstance
    ) {}

    public async getLocais() {
        try {
            const { data } = await this.client.get('/passo02/default.asp', {timeout: 30000});
            const root = parse(data);
            const select =  root.querySelector('#selUnidade');
            const options = select.querySelectorAll('option');

            return Array.from(options).slice(1).map(opt => new Local(opt));
        } catch(error) {
            throw error;
        }
    }
}