import { LocalGrupoService } from './services/localGrupo.service';
import { GrupoService } from './services/grupo.service';
import { LocalService } from './services/local.service';

const express = require('express');
const bodyParser = require('body-parser');

import axiosRetry from 'axios-retry';
import { setup } from 'axios-cache-adapter';

const PORT = process.env.PORT || 3000;

export class Main {
    private localService: LocalService;
    private grupoService: GrupoService;
    private localGrupoService: LocalGrupoService;

    constructor(

    ){   
        const client = setup({
            baseURL: 'http://gti.serra.es.gov.br/saude', 
            timeout: 5000,
            cache: {
                maxAge: 1 * 60 * 1000
            }
        });
        
        axiosRetry(client, {retries: 3});

        this.localService = new LocalService(client);
        this.grupoService = new GrupoService(client);
        this.localGrupoService = new LocalGrupoService(this.localService, this.grupoService);
    }

    public async main() {

        const app = express();
        app.use(bodyParser.json());


        app.options('/', (req: any, res: any) => {
            res.set('Access-Control-Allow-Methods', 'GET');
            res.set('Access-Control-Allow-Headers', 'Content-Type');
            res.set('Access-Control-Max-Age', '3600');
            res.status(204).send('');
        });
        app.get('/', async (req: any, res: any) => {
            const retorno = await this.localGrupoService.getDisponiveis();

            if (retorno.logErro) process.stderr.write(retorno.logErro+'\n');
            
            res.set('Access-Control-Allow-Origin', '*');
            res.send(retorno);

        })

        app.listen(PORT, '0.0.0.0', () => {
            console.log('Servidor iniciado na porta', PORT);
        })
    }
}

if (require.main === module) {
    new Main().main();
}