const datanasc_default = "01/01/1970";

export const getAge = (dateString: string = datanasc_default) => {
    const birthday = +new Date(dateString);

    return ~~((Date.now() - birthday) / 31557600000);
}